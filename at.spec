Summary:	Job spooling tools
Name:		at
Version:	3.1.12
Release:	%mkrel 4
License:	GPL
Group:		System/Servers
Source0:	http://ftp.debian.org/debian/pool/main/a/at/at_%{version}.orig.tar.gz
Url:		http://qa.mandriva.com
Source1:	atd.init
Source2:    pam.atd
Source3:    atd.sysconfig
Patch3:		at-3.1.7-sigchld.patch
Patch4:		at-3.1.8-noroot.patch
Patch9:		at-3.1.8-shell.patch
Patch10:	at-3.1.12-parallel-build.patch
Requires(post):	coreutils chkconfig /etc/init.d rpm-helper
Requires(preun):  coreutils chkconfig /etc/init.d rpm-helper
Conflicts:	crontabs <= 1.5
Buildroot:	%{_tmppath}/%{name}-%{version}-%{release}-buildroot
Requires:	common-licenses
BuildRequires:	autoconf automake flex gcc python sendmail-command
BuildRequires:	bison vixie-cron
BuildRequires:  pam-devel

%description
At and batch read commands from standard input or from a specified file.
At allows you to specify that a command will be run at a particular time
(now or a specified time in the future).  Batch will execute commands
when the system load levels drop to a particular level.  Both commands
use /bin/sh to run the commands.

You should install the at package if you need a utility that will do
time-oriented job control.  Note: you should use crontab instead, if it is
a recurring job that will need to be repeated at the same time every
day/week/etc.

%prep
%setup -q
%patch3 -p1 -b .sigchld
%patch4 -p0 -b .noroot
%patch9 -p0 -b .shell
%patch10 -p0 -b .parallel

%build
autoreconf -fi
%serverbuild
%configure2_5x --with-atspool=/var/spool/at/spool --with-jobdir=/var/spool/at

make

%install
rm -rf $RPM_BUILD_ROOT 
mkdir -p $RPM_BUILD_ROOT/{%{_initrddir},%{_bindir},%{_sbindir},%{_mandir}/man{1,5,8}}

make install IROOT=$RPM_BUILD_ROOT DAEMON_USERNAME=`id -nu` \
	DAEMON_GROUPNAME=`id -ng` \
    atdocdir=%_docdir/at

echo > $RPM_BUILD_ROOT/%{_sysconfdir}/at.deny
%{__cp} -a %{SOURCE1} %{buildroot}%{_initrddir}/atd
chmod 755 $RPM_BUILD_ROOT%{_initrddir}/atd

mkdir -p $RPM_BUILD_ROOT/%{_sysconfdir}/pam.d
install -m 644 %SOURCE2 $RPM_BUILD_ROOT/%{_sysconfdir}/pam.d/atd

install -D -m 644 %SOURCE3 %buildroot/%{_sysconfdir}/sysconfig/atd

%clean
rm -rf $RPM_BUILD_ROOT

%post
touch /var/spool/at/.SEQ
chmod 660 /var/spool/at/.SEQ
chown daemon.daemon /var/spool/at/.SEQ

%_post_service atd

%preun
%_preun_service atd

%files
%defattr(-,root,root)
%doc ChangeLog Problems README Copyright timespec
%attr(0640,root,daemon) %config(noreplace) %{_sysconfdir}/at.deny
%config(noreplace) %_sysconfdir/sysconfig/atd
%{_initrddir}/atd
%{_sysconfdir}/pam.d/atd
%attr(0770,daemon,daemon)	%dir /var/spool/at
%attr(0660,daemon,daemon)	%verify(not md5 size mtime) %ghost /var/spool/at/.SEQ
%attr(0770,daemon,daemon)	%dir /var/spool/at/spool
%{_sbindir}/atrun
%{_sbindir}/atd
%attr(6755,daemon,daemon) %{_bindir}/batch
%attr(6755,daemon,daemon) %{_bindir}/atrm
%attr(6755,daemon,daemon)   %{_bindir}/at
%{_bindir}/atq
%{_mandir}/*/atrun.8*
%{_mandir}/*/atd.8*
%{_mandir}/*/at.1*
%{_mandir}/*/atq.1*
%{_mandir}/*/atrm.1*
%{_mandir}/*/batch.1*
%{_mandir}/*/at_allow.5*
%{_mandir}/*/at_deny.5*


%changelog
* Mon May 02 2011 Oden Eriksson <oeriksson@mandriva.com> 3.1.12-4mdv2011.0
+ Revision: 662883
- mass rebuild

* Tue Nov 30 2010 Oden Eriksson <oeriksson@mandriva.com> 3.1.12-3mdv2011.0
+ Revision: 603475
- rebuild

* Mon Dec 07 2009 Pascal Terjan <pterjan@mandriva.org> 3.1.12-2mdv2010.1
+ Revision: 474425
- Fix parallel build

  + Funda Wang <fwang@mandriva.org>
    - use configure2_5x

* Mon Dec 07 2009 Olivier Thauvin <nanardon@mandriva.org> 3.1.12-1mdv2010.1
+ Revision: 474339
- 3.1.12

* Fri Nov 13 2009 Olivier Thauvin <nanardon@mandriva.org> 3.1.11-3mdv2010.1
+ Revision: 465896
- 3.1.11, remove merged patch

* Wed Sep 02 2009 Christophe Fergeau <cfergeau@mandriva.com> 3.1.10.2-3mdv2010.0
+ Revision: 424370
- rebuild

  + Oden Eriksson <oeriksson@mandriva.com>
    - rebuild

* Sun Feb 22 2009 Olivier Thauvin <nanardon@mandriva.org> 3.1.10.2-1mdv2009.1
+ Revision: 344001
- 3.1.10.2

* Sat Dec 13 2008 Olivier Thauvin <nanardon@mandriva.org> 3.1.10.1-3mdv2009.1
+ Revision: 313901
- make the load under which batch job are launch configurable, default is now N CPUs - 0.2

* Mon Oct 27 2008 Olivier Thauvin <nanardon@mandriva.org> 3.1.10.1-2mdv2009.1
+ Revision: 297720
- use pam
- fix #45066: eg make at usuable again for non root users

* Fri Aug 15 2008 Olivier Thauvin <nanardon@mandriva.org> 3.1.10.1-1mdv2009.0
+ Revision: 272478
- 3.1.10.1

* Mon Jun 16 2008 Thierry Vignaud <tv@mandriva.org> 3.1.8-25mdv2009.0
+ Revision: 220463
- rebuild

* Fri Jan 11 2008 Thierry Vignaud <tv@mandriva.org> 3.1.8-24mdv2008.1
+ Revision: 148875
- rebuild
- kill re-definition of %%buildroot on Pixel's request

  + Olivier Blin <oblin@mandriva.com>
    - restore BuildRoot

* Thu Aug 23 2007 Thierry Vignaud <tv@mandriva.org> 3.1.8-23mdv2008.0
+ Revision: 69909
- fileutils, sh-utils & textutils have been obsoleted by coreutils a long time ago

* Wed Jun 27 2007 Andreas Hasenack <andreas@mandriva.com> 3.1.8-22mdv2008.0
+ Revision: 45082
- rebuild with new serverbuild macro (-fstack-protector-all)

  + Anssi Hannula <anssi@mandriva.org>
    - rebuild with correct optflags


* Sat Jan 06 2007 David Walluck <walluck@mandriva.org> 3.1.8-20mdv2007.0
+ Revision: 104958
- rebuild
  bunzip2 patches
  fix install
- Import at

* Sat May 13 2006 Stefan van der Eijk <stefan@eijk.nu> 3.1.8-19mdk
- rebuild for sparc

* Sun Jan 08 2006 Olivier Blin <oblin@mandriva.com> 3.1.8-18mdk
- convert parallel init to LSB

* Sat Dec 31 2005 Couriousous <couriousous@mandriva.org> 3.1.8-17mdk
- Add parallel init stuff

* Fri Aug 19 2005 Per Øyvind Karlsen <pkarlsen@mandriva.com> 3.1.8-16mdk
- fix buildrequires

* Fri Aug 12 2005 Nicolas L�cureuil <neoclust@mandriva.org> 3.1.8-15mdk
- fix rpmlint errors (PreReq)

* Thu Aug 11 2005 Nicolas Lécureuil <neoclust@mandriva.org> 3.1.8-14mdk
- fix rpmlint errors (PreReq)

* Wed Aug 10 2005 Warly <warly@mandriva.com> 3.1.8-13mdk
- change smtpdaemon require to sendmail-command

* Mon Jan 10 2005 Frederic Lepied <flepied@mandrakesoft.com> 3.1.8-12mdk
- BuildRequires vixie-cron for /var/spool/cron

* Thu Sep 09 2004 Pixel <pixel@mandrakesoft.com> 3.1.8-11mdk
- don't require "mailx" anymore 
  (otherwise we have at->mailx->smtpdaemon->postfix and postfix is installed by default)

* Wed Jun 09 2004 Per Øyvind Karlsen <peroyvind@linux-mandrake.com> 3.1.8-10mdk
- fix buildrequires
- do parallell build

* Thu Jan 08 2004 Olivier Thauvin <thauvin@aerov.jussieu.fr> 3.1.8-9mdk
- fix unpackaged files

